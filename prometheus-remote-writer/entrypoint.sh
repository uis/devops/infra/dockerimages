#!/bin/sh

set -e

umask 0077

if [ -z "${GRAPHITE_PREFIX}" ]; then
  echo "!! GRAPHITE_PREFIX must be set." >&2
  exit 1
fi

if [ -z "${GRAPHITE_ADDRESS}" ]; then
  echo "!! GRAPHITE_ADDRESS must be set." >&2
  exit 1
fi

if [ -z "${GRAPHITE_PORT}" ]; then
  echo "!! GRAPHITE_PORT must be set." >&2
  exit 1
fi

echo "-- Starting prometheus exporter"
echo "   Running - /root/remote_storage_adapter --graphite-prefix=${GRAPHITE_PREFIX} --graphite-address=${GRAPHITE_ADDRESS}:${GRAPHITE_PORT}"
exec /root/remote_storage_adapter --graphite-prefix=${GRAPHITE_PREFIX} --graphite-address=${GRAPHITE_ADDRESS}:${GRAPHITE_PORT}
