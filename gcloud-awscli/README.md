# Gcloud and AWS CLI in one image

This image is based on `ubuntu:22.04` with

* latest aws cli package
* latest gcloud package

To be used for CI jobs in gitlab-services repository, performing GCS to S3 sync.

## Tags

To see the list of tags available visit the [CI job](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/-/blob/master/.gitlab-ci.yml)

## Usage

Use like any upstream image:

```Dockerfile
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/gcloud-awscli:latest

# ...
```
