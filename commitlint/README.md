# Docker image containing commitlint tool

This docker image contains the commitlint tool and a default `.commitlintrc`
file. The image is primarily to be used in GitLab CI pipelines.

The default `.commitlintrc` file is saved to `/opt/devops/.commitlintrc` and our
`commitlint.yml` CI template will use this config if one is not present in the
root of the git repo.
