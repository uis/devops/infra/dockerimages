# Pre-commit

This builds a container with [pre-commit](https://pre-commit.com/) pre-installed along with common
dependencies.

The image also includes a Java runtime for Java-based hooks.
