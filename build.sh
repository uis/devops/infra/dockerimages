#! /bin/bash

set -ex

# A path to write image metadata to.
metadata_path=$(mktemp -t metadata.XXXXXX)
trap 'rm -f $metadata_path' EXIT

# See https://gitlab.com/gitlab-org/gitlab/-/issues/389577 for info on why "--provenance false" is required.
docker_args=(buildx build --platform "$IMAGE_ARCHS" --provenance false --metadata-file "$metadata_path")

if [[ -n "$BUILD_ARGS" ]]; then
    for a in $BUILD_ARGS; do
        docker_args+=(--build-arg "$a")
    done
fi

if [[ "$PUSH_GITLAB" == "True" ]]; then
    image_folder=$CI_REGISTRY_IMAGE
    if [[ "$CI_COMMIT_REF_NAME" != "$ONLY_PUSH_ON_BRANCH" ]]; then
        if [[ "$PUSH_TEST_IMAGES" == "True" ]]; then
            summary+="Branch '$CI_COMMIT_REF_NAME' is not '$ONLY_PUSH_ON_BRANCH' and PUSH_TEST_IMAGES is '$PUSH_TEST_IMAGES' so pushing to 'test'.\n"
            image_folder="$image_folder/test"
        else
            summary+="Not pushing to GitLab: Branch '$CI_COMMIT_REF_NAME' is not '$ONLY_PUSH_ON_BRANCH' and PUSH_TEST_IMAGES is '$PUSH_TEST_IMAGES'.\n"
            unset image_folder
        fi
    fi
    if [[ -n "$image_folder" ]]; then
        echo "Registry login with $CI_REGISTRY_USER"
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"

        docker_args+=(--push)
        for tag in $TAGS; do
          image_tag="$image_folder/$IMAGE_NAME:$tag"
          summary+="Pushed tag: $image_tag\n"
          docker_args+=(--tag "$image_tag")
        done
    fi
else
    summary+="Not pushing to GitLab: GitLab pushes are disabled for this build.\n"
fi

if [[ "$PUSH_DOCKER" == "True" ]]; then
    if [[ "$CI_COMMIT_REF_NAME" == "$ONLY_PUSH_ON_BRANCH" ]]; then
        if [[ -n "$DOCKER_USER" ]] && [[ -n "$DOCKER_PASS" ]]; then
            echo "Docker Hub login"
            docker login -u "$DOCKER_USER" -p "$DOCKER_PASS"

            if [[ ! "${docker_args[*]}" =~ "--push" ]]; then
                docker_args+=(--push)
            fi

            for tag in $TAGS; do
                image_tag="$DOCKER_ORG/$IMAGE_NAME:$tag"
                summary+="Pushed tag: $image_tag\n"
                docker_args+=(--tag "$image_tag")
            done
        else
            summary+="Not pushing to Docker Hub: no DOCKER_USER and DOCKER_PASS variables.\n"
        fi
    else
        summary+="Not pushing to Docker Hub: branch '$CI_COMMIT_REF_NAME' is not '$ONLY_PUSH_ON_BRANCH'.\n"
    fi
else
    summary+="Not pushing to Docker Hub: Docker Hub pushes are disabled for this build.\n"
fi

if [[ ! "${docker_args[*]}" =~ "--tag" ]]; then
    docker_args+=(--tag "$IMAGE_TYPE")
    local_build_only=1
fi

docker_args+=("$IMAGE_TYPE")
docker "${docker_args[@]}"

# Show build metadata.
(set +x; echo "Build metadata:"; cat "$metadata_path"; echo)

# The following code exists to perform what *should* be a relatively easy check: did we actually
# build images for all the platforms we were asked to? (This may not be the case if the base images
# used in the Dockerfiles do not exist for the target platform.)
#
# It isn't as easy as you might think because there's no real way to get hold of the built images
# without pushing and re-pulling them. This is, in turn, because buildx lives in its own little
# world and, for multi platform images, only supports pushing them directly to a registry rather
# than loading them into the host docker implementation.
#
# We did originally re-pull the image but without using the SHA256 hash to specify exactly whcih
# image we wanted. This was racy as under load the GitLab registry there is a small delay between a
# successful push and the new image replacing any existing one associated with the tag. This small
# delay was, however, long enough that if we tried pulling the image immediately after pushing, we'd
# sometimes get the wrong one back.
#
# Instead, the platform of an image *is* recorded in the image manifest. For single platform images,
# this is handily written out for us by buildx to the build metadata file. For multi platform
# images, we need to be a bit cleverer and fetch the manifest we just uploaded back from the
# registry. We don't run into the same race as before, however, since this time we can use the
# SHA256 digest to make sure we're fetching the manifest corresponding to the build done in this
# build job.
#
# In one final twist of the knife, depending on exactly what combination of platforms and
# push-related options have been specified, the build metadata may be empty, refer to a manifest
# *list* (in the case of multi platform builds) or refer to an ordinary manifest (in the case of
# single platform builds). Depending on exactly which of these were the case, we need to do
# different things to get the list of platforms which were actually built.
#
# In all cases we want to get the media type of what was actually pushed. The build metadata may be
# empty or have a form we don't expect and so have a fallback value of "UNKNOWN".
pushed_media_type=$(jq -r '."containerimage.descriptor".mediaType // "UNKNOWN"' "$metadata_path")

# A file which we will write all platforms which were built to.
built_platforms_path=$(mktemp -t platforms.XXXXXX)
trap 'rm -f $built_platforms_path' EXIT

# What we do now depends on the media type of what we pushed.
case "$pushed_media_type" in
    UNKNOWN)
        # In this case we probably didn't push anything, which is OK if we're building locally.
        if [[ -z "$local_build_only" ]]; then
            echo "Could not extract pushed media type from metadata." >&2
            exit 1
        fi
        ;;
    'application/vnd.docker.distribution.manifest.v2+json')
        # This is a pretty easy case. There was only one image and the platform is recorded in the
        # build metadata itself. Happy days!
        jq -r '."containerimage.descriptor".platform.os + "/" + ."containerimage.descriptor".platform.architecture' \
                "$metadata_path" >"$built_platforms_path"
        ;;
    'application/vnd.docker.distribution.manifest.list.v2+json')
        # This case is a bit more complex. We need to grab the manifest which was pushed to the
        # repository *back* via the `docker manifest` command in order to extract the platforms. The
        # build metadata will list all of the image names. We only need the first one to pull back
        # the manifest.
        pushed_digest=$(jq -r '."containerimage.digest" // ""' "$metadata_path")
        pushed_image_name=$(jq -r '(."image.name" // "") | split(",")[0] // ""' "$metadata_path")

        # A temporary file used to store the fetched manifest.
        manifest_path=$(mktemp -t manifest.XXXXXX)
        trap 'rm -f $manifest_path' EXIT

        # Fetch the manifest and parse out the list of platforms from it.
        docker manifest inspect "$pushed_image_name@$pushed_digest" >"$manifest_path"
        (set +x; echo "Pushed manifest:"; cat "$manifest_path"; echo)
        jq -r '.manifests | map(.platform.os + "/" + .platform.architecture) | join("\n")' \
            "$manifest_path" >"$built_platforms_path"
        ;;
    *)
        # We don't understand any other media types.
        echo "Unexpected pushed media type: $pushed_media_type." >&2
        exit 1
        ;;
esac

# Since we need to have actually *pushed* something to know which platforms were actually built,
# we disable the platform checks for local builds. (Docker buildx is nothing if not "interesting" in
# that is makes finding out what it actually built *quite* so hard.)
if [[ -z "$local_build_only" ]]; then
    (set +x; echo "Platforms which were actually built:"; cat "$built_platforms_path"; echo)

    # We can now perform the comparatively simple task of checking that we built each platform we
    # were asked to.
    IFS=',' read -ra image_archs_array <<< "$IMAGE_ARCHS"
    for image_arch in "${image_archs_array[@]}"; do
        grep -Fqx "$image_arch" "$built_platforms_path" || (set +x; echo "No image built for architecture: $image_arch" >&2; exit 1)
    done
fi

# ANSI colour escape codes.
magenta='\033[1;35m'
nc='\033[0m' # No Color

echo -e "$magenta$summary$nc"
