"""Utility functions for dealing with versions."""

import itertools
from typing import Any, Iterable, Optional, Union

from packaging.version import Version as PEP440Version
from packaging.version import parse as parse_pep440_version
from semver.version import Version as SemverVersion

__all__ = [
    "most_recent_minor_semver_releases",
    "most_recent_minor_pep440_releases",
    "is_less_than_pep440_version",
    "is_less_than_or_equal_to_pep440_version",
    "is_greater_than_pep440_version",
    "is_greater_than_or_equal_to_pep440_version",
    "SemverVersionLike",
    "PEP440VersionLike",
]

SemverVersionLike = Union[SemverVersion, str]
PEP440VersionLike = Union[PEP440Version, str]


def _as_semver_version(version: SemverVersionLike, **kwargs) -> SemverVersion:
    if isinstance(version, SemverVersion):
        return version
    return SemverVersion.parse(version, **kwargs)


def _as_pep440_version(version: PEP440VersionLike) -> PEP440Version:
    if isinstance(version, PEP440Version):
        return version
    return parse_pep440_version(version)


def is_greater_than_or_equal_to_pep440_version(a: PEP440VersionLike, b: PEP440VersionLike):
    return _as_pep440_version(a) >= _as_pep440_version(b)


def is_greater_than_pep440_version(a: PEP440VersionLike, b: PEP440VersionLike):
    return _as_pep440_version(a) > _as_pep440_version(b)


def is_less_than_or_equal_to_pep440_version(a: PEP440VersionLike, b: PEP440VersionLike):
    return _as_pep440_version(a) <= _as_pep440_version(b)


def is_less_than_pep440_version(a: PEP440VersionLike, b: PEP440VersionLike):
    return _as_pep440_version(a) < _as_pep440_version(b)


def most_recent_minor_semver_releases(
    versions: Iterable[SemverVersionLike],
    *,
    not_before: Optional[SemverVersionLike] = None,
    not_after: Optional[SemverVersionLike] = None,
    exclude_build: bool = True,
    exclude_prerelease: bool = True,
    parse_kwargs: Optional[dict[str, Any]] = None,
) -> list[SemverVersion]:
    """Take an iterable of version-like objects and return the most recent minor releases from
    them. Optionally filter to those at or following *not_before* and/or those at or preceding
    *not_after*.

    If *exclude_build* is True, then "build" versions, e.g. 1.2.3+dev1, etc are excluded.

    If *exclude_prerelease* is True, then "prerelease" versions, e.g. 1.2.3-alpha1, etc are
    excluded.

    If *parse_kwargs* is provided, they are keyword args passed to semver.Version.parse().

    The returned list is sorted from newest to oldest.
    """
    # Make sure everything is a semver version.
    parse_kwargs = parse_kwargs if parse_kwargs is not None else {}
    versions = [_as_semver_version(v, **parse_kwargs) for v in versions]
    not_before = (
        _as_semver_version(not_before, **parse_kwargs) if not_before is not None else not_before
    )
    not_after = (
        _as_semver_version(not_after, **parse_kwargs) if not_after is not None else not_after
    )

    # Remove versions which are too old/too new or which are otherwise excluded.
    if not_before is not None:
        versions = [v for v in versions if v >= not_before]
    if not_after is not None:
        versions = [v for v in versions if v <= not_after]
    if exclude_build:
        versions = [v for v in versions if v.build is None]
    if exclude_prerelease:
        versions = [v for v in versions if v.prerelease is None]

    # Sort versions so that newest versions are first.
    versions.sort(reverse=True)

    # Return the most recent from each group of versions, grouped by major and minor number pairs.
    return [list(vs)[0] for _, vs in itertools.groupby(versions, lambda v: (v[0], v[1]))]


def most_recent_minor_pep440_releases(
    versions: Iterable[PEP440VersionLike],
    *,
    not_before: Optional[PEP440VersionLike] = None,
    not_after: Optional[PEP440VersionLike] = None,
    exclude_postrelease: bool = True,
    exclude_prerelease: bool = True,
    exclude_devrelease: bool = True,
) -> list[PEP440Version]:
    """Take an iterable of version-like objects and return the most recent minor releases from
    them. Optionally filter to those at or following *not_before* and/or those at or preceding
    *not_after*.

    If *exclude_{post,pre,dev}release* is True, then {post,pre,dev}release versions are excluded.

    The returned list is sorted from newest to oldest.
    """
    # Make sure everything is a semver version.
    versions = [_as_pep440_version(v) for v in versions]
    not_before = _as_pep440_version(not_before) if not_before is not None else not_before
    not_after = _as_pep440_version(not_after) if not_after is not None else not_after

    # Remove versions which are too old/too new or which are otherwise excluded.
    if not_before is not None:
        versions = [v for v in versions if v >= not_before]
    if not_after is not None:
        versions = [v for v in versions if v <= not_after]
    if exclude_prerelease:
        versions = [v for v in versions if not v.is_prerelease]
    if exclude_postrelease:
        versions = [v for v in versions if not v.is_postrelease]
    if exclude_devrelease:
        versions = [v for v in versions if not v.is_devrelease]

    # Sort versions so that newest versions are first.
    versions.sort(reverse=True)

    # Return the most recent from each group of versions, grouped by major and minor number pairs.
    return [list(vs)[0] for _, vs in itertools.groupby(versions, lambda v: (v.major, v.minor))]
