"""Utilities for dealing with PyPI."""
from urllib.parse import quote, urljoin

import requests
from packaging.version import Version, parse

__all__ = ["all_versions_for_pypi_package"]

_PYPI_API_BASE_URL = "https://pypi.org/pypi/"


def all_versions_for_pypi_package(package_name: str) -> list[Version]:
    """Return a list of all version numbers parsed for the given package name."""
    json_url = urljoin(_PYPI_API_BASE_URL, f"{quote(package_name)}/json")
    r = requests.get(json_url)
    r.raise_for_status()
    return [parse(v) for v in r.json().get("releases", {}).keys()]
