"""Utilities for dealing with Python releases."""
import re

import requests
from packaging.version import Version, parse

__all__ = ["all_python_versions"]

_PYTHON_RELEASE_API_URL = "https://www.python.org/api/v2/downloads/release/"
_PYTHON_IMAGES_TAGS_URL = (
    "https://hub.docker.com/v2/repositories/library/python/tags/?page_size=100"
)
_PYTHON_RELEASE_NAME_PATTERN = re.compile("^Python ([^ ]+)$")


def get_all_python_docker_tags() -> set[str]:
    """Return a list of all published Python docker images tags."""
    all_python_docker_tags = set()
    next_url = _PYTHON_IMAGES_TAGS_URL
    while next_url:
        response = requests.get(next_url)
        if response.status_code == 200:
            data = response.json()
            all_python_docker_tags.update([tag["name"] for tag in data["results"]])
            next_url = data.get("next", None)
        else:
            response.raise_for_status()
            break

    return all_python_docker_tags


def all_python_versions() -> list[Version]:
    """Return a list of all published Python versions."""
    python_docker_tags = get_all_python_docker_tags()
    r = requests.get(_PYTHON_RELEASE_API_URL)
    r.raise_for_status()
    return [
        parse(name_match.group(1))
        for name_match in (
            _PYTHON_RELEASE_NAME_PATTERN.match(release["name"]) for release in r.json()
        )
        if (name_match and name_match.group(1) in python_docker_tags)
    ]
