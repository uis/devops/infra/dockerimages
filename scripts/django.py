import itertools
import os

from packaging.version import Version, parse

from common import (
    all_python_versions,
    all_versions_for_pypi_package,
    most_recent_minor_pep440_releases,
)
from python import BASE_REQUIREMENTS

MINIMUM_DJANGO_VERSION = "3.2"  # earliest LTS release of Django
MINIMUM_PYTHON_VERSION = "3.8"
FLAVOURS = ["alpine"]

DJANGO_REQUIREMENTS = BASE_REQUIREMENTS + [
    "gunicorn",
    "whitenoise",
    "brotlipy",
]


def _django_is_compatible_with(django_version: Version, python_version: Version) -> bool:
    """Encode Django version compatibilties."""
    # Django 5.x requires Python 3.10 or higher.
    # (https://docs.djangoproject.com/en/dev/releases/5.0/)
    if django_version >= parse("5.0"):
        return python_version >= parse("3.10")

    # Otherwise, assume minimum Python version.
    return python_version >= parse(MINIMUM_PYTHON_VERSION)


def _generate_jobs(build_combinations):
    most_recent_python_ver = max(*(pv for _, pv in build_combinations))
    most_recent_django_ver = max(*(pv for _, pv in build_combinations))

    jobs = {}
    preferred_flavour = FLAVOURS[0]
    for django_ver, python_ver in build_combinations:
        django_slug = f"{django_ver.major}.{django_ver.minor}"
        python_slug = f"{python_ver.major}.{python_ver.minor}"
        for flavour in FLAVOURS:
            job_name = f"django-{django_slug}-python-{python_slug}-{flavour}"
            scan_job_name = (
                f"django-{django_slug}-python-{python_slug}-{flavour}-container-scanning"
            )
            tags = [f"{django_slug}-py{python_slug}-{flavour}"]
            if flavour == preferred_flavour:
                tags.append(f"{django_slug}-py{python_slug}")
            if python_ver == most_recent_python_ver:
                tags.append(f"{django_slug}-{flavour}")
                if flavour == preferred_flavour:
                    tags.append(django_slug)
                if django_ver == most_recent_django_ver:
                    tags.append(flavour)
                    if flavour == preferred_flavour:
                        tags.append("latest")

            jobs[job_name] = {
                "extends": ".build_and_push",
                "variables": {
                    "IMAGE_TYPE": f"python-{flavour}",
                    "IMAGE_NAME": "django",
                    "TAGS": " ".join(tags),
                    "REQUIREMENTS": "django",
                    "WHEN_TO_RUN": "weekly",
                    "PUSH_TEST_IMAGES": os.getenv("PUSH_TEST_IMAGES"),
                    "PYTHON_VERSION": str(python_ver),
                    "EXTRA_REQUIREMENTS": f"django=={django_ver} {' '.join(DJANGO_REQUIREMENTS)}",
                    "BUILD_ARGS": "PYTHON_VERSION EXTRA_REQUIREMENTS",
                },
                "resource_group": "django-image-build",
                "rules": [
                    {"if": "$CI_PIPELINE_SOURCE == 'parent_pipeline'"},
                ],
            }
            jobs[scan_job_name] = {
                "extends": ".container-scanning",
                "variables": {
                    "SCAN_IMAGE_NAME": "django",
                    "SCAN_IMAGE_TAG": tags[0],
                    "WHEN_TO_RUN": "weekly",
                },
                "resource_group": "django-image-scan",
                "needs": [job_name],
                "rules": [
                    {"if": "$CI_PIPELINE_SOURCE == 'parent_pipeline'"},
                ],
            }

    return jobs


def generate_django_jobs():
    python_versions_to_build = most_recent_minor_pep440_releases(
        all_python_versions(), not_before=MINIMUM_PYTHON_VERSION
    )
    django_versions_to_build = most_recent_minor_pep440_releases(
        all_versions_for_pypi_package("django"), not_before=MINIMUM_DJANGO_VERSION
    )
    build_combinations = list(
        (dv, pv)
        for dv, pv in itertools.product(django_versions_to_build, python_versions_to_build)
        if _django_is_compatible_with(dv, pv)
    )
    print("Generating jobs for the following (Django, Python) versions: ")
    for dv, pv in build_combinations:
        print(f"  - Django {dv}, Python {pv}")

    return _generate_jobs(build_combinations)
