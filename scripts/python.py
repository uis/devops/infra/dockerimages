import os

from packaging.version import parse

from common import (
    all_python_versions,
    is_greater_than_or_equal_to_pep440_version,
    is_less_than_pep440_version,
    most_recent_minor_pep440_releases,
)

MINIMUM_VERSION = "3.8"
MINIMUM_ARM64_VERSION = "3.9"
FLAVOURS = ["alpine", "slim"]
ARM64_FLAVOURS = ["slim"]

BASE_REQUIREMENTS = [
    "psycopg2-binary",
    "psycopg[binary]",
    "cffi",
    "cryptography",
    "lxml",
    "google-api-core[grpc]",
]

# A list of tuples giving the minimum (inclusive) and maximum (exclusive) version ranges where the
# corresponding apt packages should be installed.
APT_PACKAGES = [
    # psycopg2-binary does not ship wheels for Python >= 3.13.
    ("3.13", None, ["gcc", "libpq-dev"]),
]


def _generate_jobs(python_versions_to_build):
    """Generate GitLab jobs.

    This function generates a dict containing GitLab job config for each of the versions that we
    have found.
    """
    jobs = {
        "include": [
            {"local": "build-and-push.gitlab-ci.yml"},
            {"template": "Jobs/Container-Scanning.gitlab-ci.yml"},
        ]
    }
    most_recent_python_ver = max(*python_versions_to_build)
    preferred_flavour = FLAVOURS[0]
    for python_ver in python_versions_to_build:
        python_slug = f"{python_ver.major}.{python_ver.minor}"
        for flavour in FLAVOURS:
            job_name = f"python-{python_slug}-{flavour}"
            scan_job_name = f"python-{python_slug}-{flavour}-container-scanning"
            tags = [f"{python_slug}-{flavour}"]
            if flavour == preferred_flavour:
                tags.append(python_slug)
            if python_ver == most_recent_python_ver:
                tags.append(flavour)
                if flavour == preferred_flavour:
                    tags.append("latest")

            if python_ver < parse(MINIMUM_ARM64_VERSION) or flavour not in ARM64_FLAVOURS:
                archs = "linux/amd64"
            else:
                archs = "linux/amd64,linux/arm64"

            build_args = ["PYTHON_VERSION", "EXTRA_REQUIREMENTS"]

            apt_packages = []
            for min_ver, max_ver, packages in APT_PACKAGES:
                if min_ver is not None and is_less_than_pep440_version(python_ver, min_ver):
                    continue
                if max_ver is not None and is_greater_than_or_equal_to_pep440_version(
                    python_ver, max_ver
                ):
                    continue
                apt_packages.extend(packages)

            if len(apt_packages) > 0:
                build_args.append("EXTRA_APT_PACKAGES")

            job = {
                "extends": ".build_and_push",
                "variables": {
                    "IMAGE_TYPE": f"python-{flavour}",
                    "IMAGE_NAME": "python",
                    "TAGS": " ".join(tags),
                    "IMAGE_ARCHS": archs,
                    "WHEN_TO_RUN": "weekly",
                    "PUSH_TEST_IMAGES": os.getenv("PUSH_TEST_IMAGES"),
                    "PYTHON_VERSION": str(python_ver),
                    "EXTRA_REQUIREMENTS": " ".join(BASE_REQUIREMENTS),
                    "EXTRA_APT_PACKAGES": " ".join(apt_packages),
                    "BUILD_ARGS": " ".join(build_args),
                },
                "resource_group": "python-image-build",
                "rules": [
                    {"if": "$CI_PIPELINE_SOURCE == 'parent_pipeline'"},
                ],
            }

            scan_job = {
                "extends": ".container-scanning",
                "variables": {
                    "SCAN_IMAGE_NAME": "python",
                    "SCAN_IMAGE_TAG": tags[0],
                    "WHEN_TO_RUN": "weekly",
                },
                "resource_group": "python-image-scan",
                "needs": [job_name],
                "rules": [
                    {"if": "$CI_PIPELINE_SOURCE == 'parent_pipeline'"},
                ],
            }

            jobs[job_name] = job
            jobs[scan_job_name] = scan_job

    return jobs


def generate_python_jobs():
    python_versions_to_build = most_recent_minor_pep440_releases(
        all_python_versions(), not_before=MINIMUM_VERSION
    )
    print(
        "Generating jobs for the following Python versions: "
        f"{', '.join(str(v) for v in python_versions_to_build)}"
    )

    return _generate_jobs(python_versions_to_build)
