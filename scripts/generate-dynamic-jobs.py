#! /usr/bin/env python3
"""Dynamically generate build jobs.

This script is used to dynamically generate the image build jobs for each minor release of various
bits of software. The job uses the release API for each bit of software to search for the latest
patch release for every minor release. It then generates a GitLab pipeline yaml file containing the
job configuration which is triggered in the main .gitlab-ci.yml pipeline.

This ensures that we:

1. Build images for new minor versions automatically.

2. Automatically use the latest patch release for any versions of that have previously been built.
"""
import yaml

from django import generate_django_jobs
from loganterraform import generate_logan_terraform_jobs
from python import generate_python_jobs


def main():
    config = {
        "include": [
            {"local": "build-and-push.gitlab-ci.yml"},
            {"template": "Jobs/Container-Scanning.gitlab-ci.yml"},
        ],
        "variables": {"CONTAINER_SCANNING_DISABLED": "1"},
    }
    config.update(generate_django_jobs())
    config.update(generate_python_jobs())
    config.update(generate_logan_terraform_jobs())
    with open("dynamic-jobs.yaml", "w") as f:
        f.write(yaml.dump(config))


if __name__ == "__main__":
    main()
