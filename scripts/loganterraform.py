import os

import requests

from common import most_recent_minor_semver_releases

RELEASE_API_URL = "https://api.releases.hashicorp.com/v1/releases/terraform"


def _generate_jobs(versions):
    """Generate GitLab jobs.

    This function generates a dict containing GitLab job config for each of the versions that we
    have found.
    """
    jobs = {}

    for version in versions:
        if len(version["builds"]) == 0:
            print(f"WARNING: Version {version['version']}: No build urls found!")
            continue
        minor = f"{version['version'].major}.{version['version'].minor}"
        jobs[f"logan-terraform-{minor}"] = {
            "extends": ".build_and_push",
            "variables": {
                "IMAGE_TYPE": "logan-terraform",
                "IMAGE_NAME": "logan-terraform",
                "TAGS": f"{minor} {version['version']}",
                "IMAGE_ARCHS": ",".join(f"{b['os']}/{b['arch']}" for b in version["builds"]),
                "TERRAFORM_VERSION": str(version["version"]),
                "TERRAFORM_BUILD_URLS": "|".join([b["url"] for b in version["builds"]]),
                "TERRAFORM_SHASUMS_URL": version["url_shasums"],
                "OP_CLI_VERSION": "2",
                "PUSH_TEST_IMAGES": os.getenv("PUSH_TEST_IMAGES"),
                "BUILD_ARGS": "TERRAFORM_VERSION TERRAFORM_SHASUMS_URL TERRAFORM_BUILD_URLS OP_CLI_VERSION",
                "WHEN_TO_RUN": "weekly",
            },
            "resource_group": "logan-terraform-image-build",
            "rules": [
                {"if": "$CI_PIPELINE_SOURCE == 'parent_pipeline'"},
            ],
        }
        jobs[f"logan-terraform-{minor}-container-scanning"] = {
            "extends": ".container-scanning",
            "variables": {
                "SCAN_IMAGE_NAME": "logan-terraform",
                "SCAN_IMAGE_TAG": f"{minor}",
                "WHEN_TO_RUN": "weekly",
            },
            "resource_group": "logan-terraform-image-scan",
            "needs": [f"logan-terraform-{minor}"],
            "rules": [
                {"if": "$CI_PIPELINE_SOURCE == 'parent_pipeline'"},
            ],
        }
    return jobs


def generate_logan_terraform_jobs():
    all_versions = []
    res = None
    more_pages = True
    while more_pages:
        params = {"limit": 20}
        if res:
            params["after"] = res[-1]["timestamp_created"]
        r = requests.get(RELEASE_API_URL, params=params)
        r.raise_for_status()
        res = r.json()
        if not res:
            more_pages = False
        all_versions.extend(res)

    versions_by_version_number = {v["version"]: v for v in all_versions}
    versions_to_build = most_recent_minor_semver_releases(
        versions_by_version_number.keys(), not_before="0.13.7"
    )
    print(
        "Generating jobs for the following terraform versions: "
        f"{', '.join(str(v) for v in versions_to_build)}"
    )

    job_specs = []
    for version_to_build in versions_to_build:
        version_spec = versions_by_version_number[str(version_to_build)]
        builds = [
            b
            for b in version_spec["builds"]
            if b["os"] == "linux" and b["arch"] in {"amd64", "arm64"}
        ]
        job_specs.append(
            {
                "version": version_to_build,
                "url_shasums": version_spec["url_shasums"],
                "builds": builds,
            }
        )
    return _generate_jobs(job_specs)
