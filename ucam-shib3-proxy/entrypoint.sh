#!/bin/bash

set -e

shib_key="/etc/shibboleth/keys/sp-key.pem"
if [ ! -f "${shib_key}" ]; then
  echo "-- Generating new shib key."
  shib-keygen -o /etc/shibboleth/keys
else
  echo "-- Using existing shib key."
fi

extra_args=""

if [ -z "${SERVER_NAME}" ]; then
  echo "!! SERVER_NAME must be set to the FQDN for the server." >&2
  exit 1
fi

if [ -z "${BACKEND_URL}" ]; then
  echo "!! BACKEND_URL must be set to the base URL to proxy." >&2
  exit 1
fi

if [ -z "${SITE_ADMIN_EMAIL}" ]; then
  echo "!! SITE_ADMIN_EMAIL must be set to the webmasters email address." >&2
  exit 1
fi

if [ ! -z "${LOOKUP_GROUP_ID}" ]; then
  echo "Adding extra restriction: lookup group id=${LOOKUP_GROUP_ID}."
  extra_args="${extra_args} -D WithLDAPGroup"
fi

# Need both SERVER_NAME FQDN and SERVER_URL, typically https://${SERVER_NAME}
# If SERVER_URL not specified then create it (ignored for localhost)
if [ -z "${SERVER_URL}" ]; then
  if [[ "${SERVER_NAME}" == "localhost" ]]; then
    echo "Using SERVER_NAME '${SERVER_NAME}' for SERVER_URL"
    export SERVER_URL=${SERVER_NAME}
  else
    echo "Constructing SERVER_URL from SERVER_NAME '${SERVER_NAME}'"
    export SERVER_URL=https://${SERVER_NAME}
  fi
fi

# If SERVER_PORT not specified assume 80 (https offloaded)
if [ -z "${SERVER_PORT}" ]; then
  export SERVER_PORT=80
fi

# Trusted proxy list provided when behind load balancer so enable remoteip module
if [ ! -z "${REMOTEIP_INTERNAL_PROXY}" ]; then
  echo "--Enabling remoteip mod: ${REMOTEIP_INTERNAL_PROXY}"
  a2enmod remoteip
fi

envsubst < "/etc/shibboleth/shibboleth2.xml.template" > "/etc/shibboleth/shibboleth2.xml"

adduser -- _shibd ssl-cert

echo "-- Starting shibd"
service shibd start

echo "-- Starting apache"
rm -f /var/run/apache2/apache2.pid
exec /usr/sbin/apachectl -D FOREGROUND ${extra_args}
