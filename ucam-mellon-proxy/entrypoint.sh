#!/bin/sh

set -e

saml_sp_key="/etc/apache2/mellon/sp-private-key.key"

if [ -z "${SERVER_NAME}" ]; then
  echo "!! SERVER_NAME must be set to the FQDN for the server." >&2
  exit 1
fi

if [ -z "${BACKEND_URL}" ]; then
  echo "!! BACKEND_URL must be set to the base URL to proxy." >&2
  exit 1
fi

if [ -z "${SITE_ADMIN_EMAIL}" ]; then
  echo "!! SITE_ADMIN_EMAIL must be set to the webmasters email address." >&2
  exit 1
fi

if [ ! -f "${saml_sp_key}" ]; then
  echo "-- Generating new SAML sp key pair."

  HOST="$(echo "$SERVER_NAME" | sed 's#^[a-z]*://\([^:/]*\).*#\1#')"

  TEMPLATEFILE="$(mktemp -t mellon_create_sp.XXXXXXXXXX)"

  cat >"$TEMPLATEFILE" <<EOF
  RANDFILE           = /dev/urandom
  [req]
  default_bits       = 2048
  default_keyfile    = privkey.pem
  distinguished_name = req_distinguished_name
  prompt             = no
  policy             = policy_anything
  [req_distinguished_name]
  commonName         = $HOST
EOF

  openssl req -utf8 -batch -config "$TEMPLATEFILE" -new -x509 -days 3652 -nodes -out "/etc/apache2/mellon/sp-cert.cert" -keyout "/etc/apache2/mellon/sp-private-key.key" >&2

  rm -f "$TEMPLATEFILE"

else
  echo "-- Using existing SAML sp key pair."
fi

wget -O /etc/apache2/mellon/idp-metadata.xml https://shib.raven.cam.ac.uk/shibboleth
chmod 644 /etc/apache2/mellon/idp-metadata.xml 

extra_args=""

if [ ! -z "${LOOKUP_GROUP_ID}" ]; then
  echo "Adding extra restriction: lookup group id=${LOOKUP_GROUP_ID}."
  extra_args="${extra_args} -D WithLDAPGroup"
fi

echo "-- Starting apache"
rm -f /var/run/apache2/apache2.pid
exec /usr/sbin/apachectl -D FOREGROUND ${extra_args}
