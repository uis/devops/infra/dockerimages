#!/usr/bin/env sh
#
# Convenience script to set up gcloud with correct credentials, etc. Requires
# the following environment variables be non-empty:
#
# GOOGLE_CI_ACCOUNT_CREDENTIALS - JSON-formatted service account credentials
# GOOGLE_PROJECT = Google project ID for default Google project

if [ -z "${GOOGLE_CI_ACCOUNT_CREDENTIALS}" ]; then
	echo "GOOGLE_CI_ACCOUNT_CREDENTIALS must be set" >&2; exit 1
fi

if [ -z "${GOOGLE_PROJECT}" ]; then
	echo "GOOGLE_PROJECT must be set" >&2; exit 1
fi

# Configure GCloud with the service account credentials.
echo "Activating service account for gcloud tool from credentials in GOOGLE_CI_ACCOUNT_CREDENTIALS variable"
CREDENTIALS=$(mktemp -t credentials.json.XXXXXX)
echo "${GOOGLE_CI_ACCOUNT_CREDENTIALS}" >"${CREDENTIALS}"
gcloud auth activate-service-account "--key-file=${CREDENTIALS}"
rm "${CREDENTIALS}"

# Set Google project
echo "Setting default gcloud project to: '${GOOGLE_PROJECT}'"
gcloud config set project "${GOOGLE_PROJECT}"

# vim:filetype=sh
