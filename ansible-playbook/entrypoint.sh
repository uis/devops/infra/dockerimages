#!/bin/sh

set -e

# Check if the APERTURE_JUMP_HOST is set, ensure that APERTURE_USER.
# Updates ~/.ssh/config with the appropriate configuration.
if [ ! -z "$APERTURE_JUMP_HOST" ]; then
    echo "$0: SSH tunneling via APERTURE enabled" >&2

    if [ -z "${APERTURE_USER}" ]; then
      echo "$0: APERTURE_USER must be set if APERTURE_JUMP_HOST is" >&2
      exit 1
    fi

    mkdir -p ~/.ssh/
    cat >~/.ssh/config <<EOL
# Route all *.private.cam.ac.uk hosts through aperture.
Host *.private.cam.ac.uk
        ProxyCommand ssh -q -W %h:%p $APERTURE_USER@aperture.uis.cam.ac.uk

# Route public Jackdaw connection via aperture
Host jackdaw*.cam.ac.uk
        ProxyCommand ssh -q -W %h:%p $APERTURE_USER@aperture.uis.cam.ac.uk

# Route public Jackdaw connection via aperture
Host lookup*.cam.ac.uk
        ProxyCommand ssh -q -W %h:%p $APERTURE_USER@aperture.uis.cam.ac.uk

Host aperture.uis.cam.ac.uk
        ControlMaster auto
        ControlPath ~/.ssh/ansible-%r@%h:%p
        ControlPersist 5m

Host *.internal.admin.cam.ac.uk
	HostName %h
	ProxyJump $APERTURE_USER@vws-lin-devops.vws.uis.private.cam.ac.uk

EOL
    chmod 0600 ~/.ssh/config
fi

#Fetching ansible roles
if [ -f "./.ansibleroles.yaml" ]; then
  echo "-- Using .ansibleroles.yaml to fetch additional roles"
  eval $(ansibleroles)
fi

exec ansible-playbook "$@"
