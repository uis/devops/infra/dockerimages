# Photo API base container (based on debian slim)

Base container used by the Photo API. This base container is abstracted from the
Photo API to allow caching of the base container and faster building of the Photo API.

## Tags

To see the list of tags available visit the [CI job](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/-/blob/master/.gitlab-ci.yml)

## Usage

Use like any upstream image:

```Dockerfile
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/photo-api-base:py3.11-fr1.3.0

# ...
```
