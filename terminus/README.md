# Terminus, the Pantheon CLI tool image

Base container used by the CI jobs in webcms. This is a `gcloud-docker` container but with
additional packages such as php, composer, etc.

## Tags

To see the list of tags available visit the [CI job](https://gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/-/blob/master/.gitlab-ci.yml)

## Usage

Use like any upstream image:

```Dockerfile
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/terminus:latest

# ...
```
