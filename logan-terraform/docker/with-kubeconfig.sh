#!/usr/bin/env sh
#
# Simple wrapper to use the terraform kubeconfig output to set an appropriate
# KUBECONFIG environment variable.

if [ -z "$TERRAFORM_KUBECONFIG_OUTPUT" ]; then
  TERRAFORM_KUBECONFIG_OUTPUT=kubeconfig_content
fi

export KUBECONFIG=$(mktemp -t kubeconfig.XXXXXXXX)
echo "Using terraform output ${TERRAFORM_KUBECONFIG_OUTPUT}"
terraform output -raw ${TERRAFORM_KUBECONFIG_OUTPUT} > "${KUBECONFIG}"
exec $@
