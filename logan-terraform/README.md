# Terraform and Helm container

This builds a container from Google's Cloud SDK alpine image, adding terraform, op-cli,
helm, and kubectl.

Google service account credentials for the terraform admin user should be mounted
at /credentials.json.

It is recommended that you mount a volume at /terraform_data to hold the
terraform data directory.

When running ``terraform init``, modules will be fetched from git repos via
SSH. In order to allow this, the ssh-agent socket should be forwarded.

## Additional wrappers

- The `with-kubeconfig.sh` wrapper script enables the use of terraform output
(by default `kubeconfig_content` but overrided by TERRAFORM_KUBECONFIG_OUTPUT env)
to set appropriate KUBECONFIG environment variable.
- The `terraform-upgrade` wrapper script is intended to make it easier to upgrade terraform
modules/providers and write provider dependency information into the dependency lock file.

## Usage

```bash
$ docker run --rm -it \
    -v ${SSH_AUTH_SOCK}:/ssh-agent -e SSH_AUTH_SOCK=/ssh-agent \
    -v terraform_gitlab_data:/terraform_data/ \
    registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/logan-terraform:latest \
    <terraform or additional wrapper command>
```

## Logan

This container can be used by [logan](https://gitlab.developers.cam.ac.uk/uis/devops/tools/logan)
tool by specifying either of the following in the repo's `.logan.yaml` file:
```
image: registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/logan-terraform`
image: uisautomation/logan-terraform
```
