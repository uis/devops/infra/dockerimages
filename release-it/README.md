# Release It

This builds a container with
[release-it](https://github.com/release-it/release-it) and associated plugins
installed. It also includes some scripts specific to the [UIS DevOps release
automation
processes](https://guidebook.devops.uis.cam.ac.uk/en/latest/explanations/gitlab-release-automation/).
