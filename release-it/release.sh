#! /usr/bin/env bash

set -e

MAGENTA="\e[35m"
CLEAR="\e[0m"

log() {
    echo -e "${MAGENTA}${1}${CLEAR}"
}

log "Running release-it to generate a new release..."
if [[ -n "$USE_MERGE_REQUEST_RELEASE_FLOW" ]]; then
    npx release-it --ci --no-npm --no-git.commit
else
    npx release-it --ci
fi
