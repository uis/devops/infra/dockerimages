#! /usr/bin/env bash

set -e

MAGENTA="\e[35m"
CLEAR="\e[0m"

log() {
    echo -e "${MAGENTA}${1}${CLEAR}"
}

branch=$(git rev-parse --abbrev-ref HEAD)
release_branch="release-it--${branch}"

if ! git log --pretty=format:%s "$(git describe --tags --abbrev=0)..HEAD" | grep -E '^(feat|fix)'; then
    log "A new release is not required."
    exit 0
fi

log "Checking out ${release_branch}..."
git checkout -B "$release_branch"
log "Running release-it..."
npx release-it --ci --no-git.tag --no-git.requireUpstream --git.pushArgs=--force --no-gitlab

log "Generating merge request title and description..."
title="Draft: $(git show -s --format=%s)"
changelog=$(npx release-it --changelog)
description=$(cat << EOF
:rocket: _This is an automated release merge request. For more information, see the [GitLab Release Automation](https://guidebook.devops.uis.cam.ac.uk/en/latest/explanations/gitlab-release-automation/) section in the Guidebook._

$changelog
EOF
)

log "Attempting to retrieve existing merge request..."
mr=$(gitlab --private-token "$GITLAB_TOKEN" --output json --fields iid project-merge-request list \
    --project-id "$CI_PROJECT_ID" \
    --state "opened" \
    --source-branch "$release_branch" \
    --target-branch "$branch"
)

if echo "$mr" | jq -e ". == []"; then
    log "Couldn't find an existing merge request for '${release_branch} -> ${branch}'."
    log "Creating merge request..."
    gitlab --private-token "$GITLAB_TOKEN" project-merge-request create \
        --project-id "$CI_PROJECT_ID" \
        --source-branch "$release_branch" \
        --target-branch "$branch" \
        --title "$title" \
        --description "$description"
else
    iid=$(echo "$mr" | jq -r ".[0].iid")
    log "Found existing merge request for ${release_branch} -> ${branch} with iid '${iid}'."
    log "Updating merge request..."
    gitlab --private-token "$GITLAB_TOKEN" project-merge-request update \
        --project-id "$CI_PROJECT_ID" \
        --iid "$iid" \
        --title "$title" \
        --description "$description"
fi

git checkout "$branch"
